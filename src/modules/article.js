import articles from "@/service/articles"


const state = {
    isLoading: null,
    data:null,
    error: null,
    ArticleDetail:null
}
const mutations = {
    getArticleStart(state){
        state.isLoading=true
        state.data=null
        state.error=null
        state.ArticleDetail=null
    },
    getArticleSuccess(state, payload){
        state.isLoading=false
        state.data=payload
    },
    getArticleFailure(state){
        state.isLoading=false
    
    },
    getArticleDetail(state){
        state.isLoading=true
        state.data=null
        state.error=null
    },
    getArticleDetailSuccess(state, payload){
        state.isLoading=false
        state.ArticleDetail=payload
    },
    getArticleDetailFailure(state){
        state.isLoading=false
    }


}
const actions = {
    get_article(context){
        return new Promise((resolve, reject) => {
            context.commit('getArticleStart')
        articles.getArticles()
        .then((response) => {
            context.commit('getArticleSuccess', response.data.articles)
            console.log("RESPONSE", response)
            resolve()
        })
        .catch((err) =>{
            reject(err)
        })
        })
    },
    get_article_detail(context, slug){
        return new Promise(() =>{
      context.commit('getArticleDetail')
      articles.getArticleDetail(slug)
      .then((response) =>{
        context.commit('getArticleDetailSuccess', response.data.article)
      })
      .catch(() =>{
        context.commit('getArticleDetailFailure')
      })
        })
    },
    delete_article(context, slug){
        console.log(slug)
        return new Promise (() =>{
            articles.deleteArticle(slug)
                .then(()=>{
                    console.log('deleted')
                })
                .catch(() =>{
                    console.log("Error in deleting")
                })
        })
    }
}


export default {
    state,
    mutations,
    actions
}
