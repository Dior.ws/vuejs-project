export const getterTypes = {
    currentUser: '[auth] nowUser',
    isLoggedIn:'[auth] isLoggedIn' ,
    isAnonymus:'[auth] isAnonymus'
}