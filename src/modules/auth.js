import AuthService from "@/service/auth"
import {removeItem, setItem} from "../helpers/persistanceStorage"
import {getterTypes} from "./types"
const state={
    isLoading:false,
    userData:{},
    errors:{},
    isLoggedIn:null,
}
const getters = {
    [getterTypes.currentUser]: state => { 
        return state.userData
        },
    [getterTypes.isLoggedIn]: state =>{
        return Boolean(state.isLoggedIn)
    },
    [getterTypes.isAnonymus]: state =>{
        return  state.isLoggedIn ===false
    }
    
}
const mutations={
    registerStart(state){
      state.isLoading=true
      state.isLoggedIn=false
    },
    registerSuccess(state, payload){
        state.isLoading=false
        state.userData=payload
        state.isLoggedIn=true
    },
    registerFailure(state, payload){
        state.isLoading=false
       state.errors=payload
       state.isLoggedIn=false
    },
    loginStart(state){
        state.isLoading=true
        state.isLoggedIn=false
    },
    loginSuccess(state, payload){
        state.isLoading=false
        state.userData=payload
        state.isLoggedIn=true
    },
    loginFailure(state, payload){
        state.isLoading=false
        state.errors=payload
        state.isLoggedIn=false
    },
    currentUserStart(state){
      state.isLoading=true
      state.isLoggedIn=false
    },
    currentUserSuccess(state, payload){
        state.isLoading=false
        state.isLoggedIn=true
        state.userData=payload
    },
    currentUserFailure(state){
        state.isLoading=false
        state.isLoggedIn=false
    },
    logOut(state){
        state.isLoggedIn=false
        state.userData=null
    }
}
const actions={
    register(context, user){
        return new Promise((resolve, reject) =>{
            context.commit('registerStart')
            AuthService.register(user)
            .then((response)=>{
                context.commit('registerSuccess', response.data.user)
                setItem('token', response.data.user.token)
                resolve(response.data)
            })
            .catch((err)=>{
                context.commit('registerFailure', err.response.data.errors)
                 reject(err)
            })
        })
       
    },
    login(context, user){
        return new Promise((resolve, reject) =>{
           context.commit('loginStart')
           AuthService.login(user)
           .then((response) =>{
            context.commit('loginSuccess', response.data.user)
            setItem('token', response.data.user.token)
            resolve(response.data)
           })
           .catch((err) =>{ 
            context.commit('loginFailure', err.response.data.errors)
            reject(err)
           })
        })
    },
    getUser(context){
        return new Promise ((resolve) =>{
            context.commit('currentUserStart')
            AuthService.getUser()
            .then((response) =>{
                context.commit('currentUserSuccess', response.data.user)
                resolve(response.data.user)
            })
            .catch((err) =>{
                context.commit('currentUserFailure')
                console.log(err)
            })
        })
    },
    logout(context){
        context.commit('logOut')
        removeItem('token')
    }
}
export default{
    state,
    mutations,
    actions,
    getters
}