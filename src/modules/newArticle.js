import createArticle from "@/service/newArticle"

const state ={
 isLoading: null,

}
const actions = {
   handle_new_article(context, newArticle){
    return new Promise(()=>{
        createArticle.newArticle(newArticle)
        .then((response) => {
            console.log("SUCCESS", response)
        })
        .catch((err) =>{
            console.log("ERROR", err)
        })
    })
   }
}
export default {
    state,
    actions
}