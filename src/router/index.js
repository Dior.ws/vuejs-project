import { createRouter, createWebHistory } from "vue-router";
import { HomeView, LoginView, RegisterView, ArticleCardDetail, CreateArticle } from "@/views";
const routes = [
    {
        path:'/',
        name:'Home',
        component:HomeView
    },
   {
    path:'/register',
    name:'register',
    component:RegisterView
   },
   {
    path:'/login',
    name:'login',
    component:LoginView
   },
   {
    path:'/articles/:id',
    name:'ArticleCardDetail',
    component:ArticleCardDetail
   },
   {
    path:'/create-article',
    name:'create-article',
    component:CreateArticle
   },
]
const router = createRouter({
    history:createWebHistory(process.env.BASE_URL),
    routes
})
export default router
