export const getItem= (key)=>{
  try{
    return JSON.parse(localStorage.getItem(key))
  } catch (error) {
   console.log('Error getting data')
  }
}

export const setItem =(key, token) =>{
    try {
        localStorage.setItem(key, JSON.stringify(token))
    } catch{
        console.log('Not set')
    }
} 
export const removeItem = (key) => {
  try {
    localStorage.removeItem(key)
  } catch {
    console.log("Error deleting token")
  }
}