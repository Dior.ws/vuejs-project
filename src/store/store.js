import { createStore } from "vuex";
import auth from "@/modules/auth";
import article from "@/modules/article";
import newArticle from '@/modules/newArticle'
export default createStore({
   state:{},
   mutations:{},
   actions:{},
   getters:{},
   modules:{auth, article, newArticle}, 
})