import axios from "./axios"

const articles = {
    getArticles(){
         return axios.get('/articles')
    },
    getArticleDetail(slug){
      return axios.get(`/articles/${slug}`)
    },
    deleteArticle(slug){
      return axios.delete(`/articles/${slug}`)
    }
}
export default articles