import axios from './axios'

const createArticle = {
    newArticle(article){
      return  axios.post('/articles', {article})
    }
}

export default createArticle